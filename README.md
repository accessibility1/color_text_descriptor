# color text descriptor

(*English language*) This app takes a photo, tries to pick a color from the scene and shows you the color as text.
It is accessible and can be usefull to blind people (or for color blindness or dichromatic perception or color vision deficiency).
The color name resulting is obtained from the average of 5 colors picked from 5 different areas. If the photo contains more than one color, you can observe a confidence lesser than 100%. If the confidence is lower than 40% you can read five different colors, one color for each area. 
The app can take photos with or without flash using two different buttons.
This application is multilanguage: for now English and Italian.

(*Italiano*) Questa applicazione scatta una foto, prova a interpretare un colore visibile nella scena e lo mostra come testo. 
È accessibile e può essere molto utile (un ausilio) alle persone non vedenti (o per chi soffre di daltonismo). 
Il colore che si ottiene è ottenuto dal calcolo della media di 5 colori prelevati da 5 diverse aree dell'immagine. Se la foto contiene più di un colore, si potrà leggere un valore di affidabilità minore del 100%. Quando tale valore è minore del 40%, si potranno leggere 5 diversi colori, uno per ognuna delle 5 diverse aree. 
L'applicazione può scattare foto con flash oppure senza flash, usando due diversi pulsanti.
Questa applicazione è tradotta in diverse lingue, per il momento in inglese ed in italiano.

## Screenshot

![screenshot color text description app](./docs/img/screenshot-1.png)

*This screenshot shows the camera permission request.*

![screenshot color text description app](./docs/img/screenshot-2.png)

*This screenshot shows the camera view with two button to tap to get the color. The second button use the flash*

## License

This is free software using MIT license

## Known issues

* All the photos are stored in temporary files, in the app cache memory (you have to manually clear the cache)

* Passing from *Camera Tab* back to *Permission Tab* and again to *Camera Tab* could turn off the camera and show a black view

## Building

### Dependencies

The React Native framework depends on:

 * openjdk 11 (or 17)
 * android sdk (>=31)
 * ios sdk 14 (it requires Xcode from the Apple App Store)

You can read the project dependencies in the following file:

[package.json](./package.json)

The latest version (beta 0.1.0) is based on this software:

 * node.js 18.4
 * react-native 0.71.3
 * react-navigation
 * react-native-vision-camera 2.15.4
 * @bam.tech/react-native-image-resizer 3.0.4
 * react-native-image-base64 0.1.4
 * react-native-image-colors 1.5.2
 * react-native-file-access 2.5.2
 * react-native-localize 2.2.6
 * react-i18next 12.2.0
 * @react-native-async-storage/async-storage 1.18.1

### Project configuration

#### Android configuration
I've built this application using Android SDK 33 on Debian GNU/Linux operating system.

 * To install all the dependencies:

    npm install


#### iOS configuration

I've build this application using *Xcode* in macOS 12 with iOS 14 (sdk)

 * To install all the dependencies:

        npm install
        npx pod-install

### Building

#### Android

    cd ./android
    ./gradlew clean
    ./gradlew buildRelease
    ./gradlew assembleRelease
    file app/build/outputs/apk/release/app-release.apk

Connect an Android smartphone to transfer the apk file

#### iOS

You can run this app in the iPhone simulator, but it is useless because the iPhone simulator lacks of camera simulator.

You can run this app in the real iPhone, but *Xcode* will require:

 * you have to belong to an Apple Developement Team (cost 99$)
 * you have to belong to a Personal Team (cost 0$)

To *build* and *install* this app, connect the iPhone to macOS machine and run these command in two different terminal windows

    npx react-native start

    #(open another ternimal)#
    npx react-native run-ios
