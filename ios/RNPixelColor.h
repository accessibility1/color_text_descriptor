//
//  RNPixelColor.h
//
// original project
// https://github.com/dudyn5ky1/react-native-get-pixel-color
// 
#import <React/RCTBridgeModule.h>

@interface RNPixelColor : NSObject <RCTBridgeModule>
@property NSUInteger bytesPerPixel;
@property NSUInteger bytesPerRow;
@property unsigned char *rawData;

@end
