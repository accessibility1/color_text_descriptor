/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import './src/i18n.js';
// import '@formatjs/intl-pluralrules/polyfill'

AppRegistry.registerComponent(appName, () => App);
