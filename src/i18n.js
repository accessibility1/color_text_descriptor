import 'intl-pluralrules';
import i18n from 'i18next';
import {languageDetectorPlugin} from './utils/languageDetectorPlugin.js';
import {initReactI18next} from 'react-i18next';
import {en, it} from './locales/index.js';

const resources = {
  en: {
    translation: en,
  },
  it: {
    translation: it,
  },
};
i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .use(languageDetectorPlugin)
  .init({
    resources,
    preload: true,
    debug: false,
    fallbackLng: 'en',
    keySeparator: false, // we do not use keys in form messages.welcome
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
    react: {
      useSuspense: false,
    },
  });
export default i18n;
