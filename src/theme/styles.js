import React from 'react';
import {StyleSheet, Image} from 'react-native';

const styles = StyleSheet.create({
  tabStyle: {
    labelStyle: {fontSize: 18},
  },
  permissionTabIcon: {
    tabBarIcon: ({focused, color, size}) => {
      if (focused) {
        return <Image source={require('../assets/icons/permission-red.png')} />;
      } else {
        return (
          <Image source={require('../assets/icons/permission-gray.png')} />
        );
      }
    },
  },
  cameraTabIcon: {
    tabBarIcon: ({focused, color, size}) => {
      if (focused) {
        return <Image source={require('../assets/icons/camera-blue.png')} />;
      } else {
        return <Image source={require('../assets/icons/camera-gray.png')} />;
      }
    },
  },
  textInfo: {
    fontSize: 18,
    fontWeight: 'bold',
    padding: 18,
  },
  navigator: {
    tabBarActiveBackgroundColor: '#DDDDDD',
    tabBarInactiveBackgroundColor: '#CCCCCC',
    tabBarActiveTintColor: '#FF0000',
    tabBarInactiveTintColor: '#990000',
  },
  container: {
    flex: 1,
  },
  buttonPosition: {
    position: 'absolute',
    top: 40,
    left: 40,
    width: '77%',
  },
  buttonFlashPosition: {
    position: 'absolute',
    top: 110,
    left: 40,
    width: '77%',
  },
  pressableText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  pressablePosition: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'blue',
  },
  myButton: {
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#38386F',
    borderRadius: 10,
    borderWidth: 2,
    borderColo: 'white',
  },
  myText: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export {styles};
