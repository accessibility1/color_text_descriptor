/**
 *
 * @format
 * @flow strict-local
 */

import {setImage, pickColorAt} from './index.js';

function setImageAndGetOnePixelColor(base_64_only_android, x, y) {
  setImage(base_64_only_android)
    .then(isSet => {
      console.log(isSet);
      //      return pickColorAt(x, y);
    })
    .then(color => {
      console.log('color : ' + color);
      return color;
    })
    .catch(error => {
      console.log(error);
    });
}

async function setImageAndroid(base_64) {
  try {
    const isSet = await setImage(base_64);
    await console.log(isSet);
    return isSet;
  } catch (error) {
    console.log(error);
  }
}

function setImageIos(path) {
  setImage(path)
    .then(isSet => {
      console.log(isSet);
      return isSet;
    })
    .catch(error => {
      console.log(error);
    });
}
async function getOnePixelColor(x, y) {
  try {
    const color = await pickColorAt(x, y);
    await console.log('color: ' + color);
    return color;
  } catch (e) {
    console.log('get1 ' + e);
  }
}

export {
  setImageAndGetOnePixelColor,
  setImageAndroid,
  setImageIos,
  getOnePixelColor,
};
