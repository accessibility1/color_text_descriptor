//utils/languageDetectorPlugin.js
// this source code was from https://dev.to/ramonak/react-native-internationalization-with-i18next-568n

import * as RNLocalize from 'react-native-localize';
import AsyncStorage from '@react-native-async-storage/async-storage';

const STORE_LANGUAGE_KEY = 'settings.lang';

const languageDetectorPlugin = {
  type: 'languageDetector',
  async: true,
  init: () => {},
  detect: async function (callback) {
    try {
      //get stored language from Async storage
      await AsyncStorage.getItem(STORE_LANGUAGE_KEY).then(language => {
        if (language) {
          //if language was stored before, use this language in the app
          return callback(language);
        } else {
          //if language was not stored yet, use device's locale
          const res = RNLocalize.getLocales();
          console.log(res);
          return callback(res[0].languageCode);
        }
      });
    } catch (error) {
      console.log('language detector plugin: Error reading language', error);
    }
  },
  cacheUserLanguage: async function (language) {
    try {
      //save a user's language choice in Async storage
      await AsyncStorage.setItem(STORE_LANGUAGE_KEY, language);
    } catch (error) {}
  },
};

export {languageDetectorPlugin};
