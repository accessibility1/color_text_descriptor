const calibration = {
  l: 1.7,
  s: 5.5,
};

const sizes = {
  portrait: {
    xmax: 80, // it should be 45, but resizing is not correct.
    ymax: 80,
  },
  landscape: {
    xmax: 80,
    ymax: 80, // it can be 45, resizing is correct
  },
};

const orientation = (width, height) => {
  if (width <= height) {
    return sizes.portrait;
  } else {
    return sizes.landscape;
  }
};

const MARGIN = 20;

const getPoints = size => {
  const z = MARGIN;
  return [
    {x: z, y: z},
    {x: size.xmax - z, y: z},
    {x: size.xmax - z, y: size.ymax - z},
    {x: z, y: size.ymax - z},
    {x: Math.floor(size.xmax / 2), y: Math.floor(size.ymax / 2)},
  ];
};

export {sizes, orientation, getPoints, calibration};
