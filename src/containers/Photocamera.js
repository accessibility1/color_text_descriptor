/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {View, Text, TouchableOpacity} from 'react-native'; //to do: replace with react-native-ui-lib if possibile
import {Platform, StyleSheet, useWindowDimensions} from 'react-native';
import {Camera, useCameraDevices} from 'react-native-vision-camera';
import {useRef, useState} from 'react';
import {styles} from '../theme/styles.js';
import {useTranslation} from 'react-i18next';
import {
  resize,
  img2base64,
  base64header,
  deletingFile,
} from '../misc/functionAsync.js';
import {
  saturation,
  colorName,
  hsl,
  convertHexToObj,
  averageColor,
  colorDistance,
  countColor,
  average4Points,
} from '../misc/colorFunction.js';
import {
  setImageAndroid,
  setImageIos,
  getOnePixelColor,
} from '../modules/pixelColor.js';
import {orientation, getPoints} from '../const/pixelPositions.js';

const PhotocameraPage = (props): Node => {
  const {t, i18n} = useTranslation();
  const buttonTitleInit = t('accessibility hint first button');
  const devices = useCameraDevices();
  const device = devices.back;
  const cameraRef = useRef();
  const [varTitle, setVarTitle] = useState(buttonTitleInit);
  const {height, width} = useWindowDimensions();

  function setterTitle(newColor) {
    if (JSON.stringify(newColor).lenght > 35) {
      // to avoid reading long error
      return;
    }
    if (JSON.stringify(varTitle) === JSON.stringify(newColor)) {
      // for accessibility on android
      newColor = newColor + '!';
    }
    setVarTitle(newColor);
  }
  const executor = async flashFlag => {
    const size = orientation(width, height); // it should have better implementation
    const colorPoints = getPoints(size);

    function snap(flash) {
      if (cameraRef.current) {
        const photo = cameraRef.current.takePhoto({flash: flash});
        return photo;
      }
    }
    try {
      const photo = await snap(flashFlag);
      if (photo.path == null) {
        throw new Error('Photo null');
      }
      const mini = await resize(photo.path);
      if (mini == null) {
        throw new Error('mini photo object null');
      }
      const base64miniature = await img2base64(mini);
      if (base64miniature == null) {
        throw new Error('base64 conversion error');
      }
      const base64typed = await base64header(base64miniature);
      if (base64typed == null) {
        throw new Error('base 64 string null');
      }
      if (Platform.OS === 'android') {
        await setImageAndroid(base64miniature); // API for android
      } else {
        await setImageIos(mini.uri); // API for ios
      }

      const arrPromises = [];
      for (let i = 0; i < 5; i++) {
        arrPromises[i] = await getOnePixelColor(
          colorPoints[i].x,
          colorPoints[i].y,
        );
      }

      await console.log(JSON.stringify(arrPromises));
      let arrObj = convertHexToObj(arrPromises);
      console.log(JSON.stringify(arrObj, null, 2));
      arrObj[5] = averageColor(arrObj);
      console.log(JSON.stringify(arrObj[5], null, 2));
      // indexMax is the index of element to exclude from the array
      let indexMax = -1;
      indexMax = colorDistance(arrObj);
      const averageRGB = average4Points(arrObj, indexMax);

      const HSLarray = hsl(averageRGB);
      const computedColor = colorName(HSLarray);
      const [adjective1, adjective2] = saturation(HSLarray);

      console.log(JSON.stringify(averageRGB, null, 2));
      console.log(JSON.stringify(HSLarray, null, 2));
      console.log(JSON.stringify(computedColor));
      const countResult = countColor(arrPromises, computedColor);
      const trust = countResult.trust;
      const arrNames = countResult.names;
      if (trust >= 40) {
        console.log(adjective1 + adjective2 + computedColor);
        setterTitle(
          t(adjective1) +
            ' ' +
            t(adjective2) +
            ' ' +
            t(computedColor) +
            ' ' +
            trust +
            '%',
        ); // STATE varTitle
      } else {
        // low trust
        const tArrNames = arrNames.map(name => t(name));
        const tTitle = tArrNames.join(' ');
        setterTitle(t(adjective1) + ' ' + t(adjective2) + ' ' + tTitle);
      }

      console.log(JSON.stringify(computedColor));

      if (computedColor == null) {
        throw new Error('color value null');
      }
      await deletingFile(photo.path);
    } catch (error) {
      console.log('error: ' + error);
    }
  };

  if (device == null) {
    return (
      <>
        <Text>loading..</Text>
      </>
    );
  }
  return (
    <>
      <Camera
        device={device}
        photo={true}
        ref={cameraRef}
        style={StyleSheet.absoluteFill}
        isActive={true}
        preset="low"
        accessible={true}
        accessibilityLabel={t('Label accessibility of camera view')}
      />
      <View style={styles.buttonPosition}>
        <TouchableOpacity
          underlayColor="#FFFFFF"
          onPress={() => executor('off')}
          title={t(JSON.stringify(varTitle))}
          accessibilityLabel={t(JSON.stringify(varTitle))}
          accessibilityHint={t('accessibility hint first button')}
          accessibilityLiveRegion="polite"
          style={styles.myButton}>
          <Text style={styles.myText}>{t(JSON.stringify(varTitle))}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonFlashPosition}>
        <TouchableOpacity
          underlayColor="#FFFFFF"
          onPress={() => executor('on')}
          title={t(JSON.stringify(varTitle))}
          accessibilityLabel={t(JSON.stringify(varTitle)) + ' + Flash'}
          accessibilityHint={t('accessibility hint second button')}
          accessibilityLiveRegion="polite"
          style={styles.myButton}>
          <Text style={styles.myText}>
            {t(JSON.stringify(varTitle)) + ' + Flash'}
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export {PhotocameraPage};
