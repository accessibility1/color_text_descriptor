/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {Text, View} from 'react-native';
import {request} from '../misc/getPermission.js';
import {styles} from '../theme/styles.js';
import {useTranslation} from 'react-i18next';

const PermissionPage = (): Node => {
  const {t, i18n} = useTranslation();
  request();
  return (
    <>
      <View>
        <Text style={styles.textInfo} accessible={true}>
          {t('Title starting page')}.
        </Text>
        <Text style={styles.textInfo} accessible={true}>
          {t('Warning about temporary files')}
        </Text>
      </View>
    </>
  );
};

export {PermissionPage};
