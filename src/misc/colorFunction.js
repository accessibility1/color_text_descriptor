import {calibration} from '../const/pixelPositions.js';

function average4Points(arrObj, indexMax) {
  const averageRed = Math.floor((arrObj[5].r * 5 - arrObj[indexMax].r) / 4);
  const averageGreen = Math.floor((arrObj[5].g * 5 - arrObj[indexMax].g) / 4);
  const averageBlue = Math.floor((arrObj[5].b * 5 - arrObj[indexMax].b) / 4);
  const averageRGB = [averageRed, averageGreen, averageBlue];
  return averageRGB;
}

function countColor(arrHex, color) {
  let result;
  const arrRGB = arrHex.map(x => {
    result = hexToArr(x);
    return result;
  });
  console.log(arrRGB);
  const arrName = arrRGB.map(x => {
    console.log(x);
    console.log(hsl(x));
    return colorName(hsl(x));
  });
  console.log(arrName);
  let counter = 0;

  for (const item of arrName.flat()) {
    if (item === color) {
      counter++;
    }
  }
  return {trust: counter * 20, names: arrName};
}

function convertHexToObj(colors) {
  console.log(JSON.stringify(colors, null, 2));
  const arrObj = colors.map(hexc => {
    console.log(hexc);
    return hexToObj(hexc);
  });

  arrObj[5] = {r: 0, g: 0, b: 0, d: 0};
  return arrObj;
}

function averageColor(arrObj) {
  for (let i = 0; i < 5; i++) {
    arrObj[5].r += arrObj[i].r;
    arrObj[5].g += arrObj[i].g;
    arrObj[5].b += arrObj[i].b;
  }
  arrObj[5].r = Math.floor(arrObj[5].r / 5);
  arrObj[5].g = Math.floor(arrObj[5].g / 5);
  arrObj[5].b = Math.floor(arrObj[5].b / 5);

  for (let i = 0; i < 5; i++) {
    arrObj[i].d = Math.sqrt(
      (arrObj[i].r - arrObj[5].r) ** 2 +
        (arrObj[i].g - arrObj[5].g) ** 2 +
        (arrObj[i].b - arrObj[5].b) ** 2,
    );
  }
  return arrObj[5];
}

function colorDistance(arrObj) {
  let indexMax = 5;
  for (let i = 0; i < 5; i++) {
    if (arrObj[i].d > arrObj[5].d) {
      indexMax = i;
      arrObj[5].d = arrObj[i].d;
    }
  }
  console.log('distance i ' + indexMax);
  return indexMax;
}

function hexToObj(hexString) {
  hexString = hexString.slice(1, 7);
  let r = hexString.slice(0, 2);
  let g = hexString.slice(2, 4);
  let b = hexString.slice(4, 6);
  let red = parseInt(r, 16);
  let green = parseInt(g, 16);
  let blue = parseInt(b, 16);
  return {r: red, g: green, b: blue};
}

function hexToArr(hexString) {
  hexString = hexString.slice(1, 7);
  console.log(hexString);
  let r = hexString.slice(0, 2);
  let g = hexString.slice(2, 4);
  let b = hexString.slice(4, 6);
  let red = parseInt(r, 16);
  let green = parseInt(g, 16);
  let blue = parseInt(b, 16);
  return [red, green, blue];
}

function hex2Dec(hexString) {
  hexString = hexString.slice(1, 7);
  let r = hexString.slice(0, 2);
  let g = hexString.slice(2, 4);
  let b = hexString.slice(4, 6);
  let red = parseInt(r, 16);
  let green = parseInt(g, 16);
  let blue = parseInt(b, 16);
  return [red, green, blue];
}

// Source Code based on:
// https://stackoverflow.com/questions/9224404/get-color-name-by-hex-or-rgb

function hsl(rgbArr) {
  const r1 = Number(rgbArr[0]) / 255,
    g1 = Number(rgbArr[1]) / 255,
    b1 = Number(rgbArr[2]) / 255;
  const Y2020 = 0.2627 * r1 + 0.678 * g1 + 0.0593 * b1;

  const maxColor = Math.max(r1, g1, b1),
    minColor = Math.min(r1, g1, b1);
  let C = maxColor - minColor;

  const I = (r1 + g1 + b1) / 3;
  let SI = 0;
  if (I === 0) {
    SI = 0;
  } else {
    SI = 1 - minColor / I;
  }

  // Luminance
  let L = (maxColor + minColor) / 2,
    S = 0,
    H = 0;
  // Saturation
  if (maxColor !== minColor) {
    if (L === 0 || L === 1) {
      S = 0;
    } else {
      S = C / (1 - Math.abs(2 * L - 1));
    }

    // Hue
    if (r1 === maxColor) {
      H = (g1 - b1) / C;
    } else if (g1 === maxColor) {
      H = 2.0 + (b1 - r1) / C;
    } else if (b1 === maxColor) {
      H = 4.0 + (r1 - g1) / C;
    }
  }
  L = Y2020 * 100;
  S = SI * 100;
  H = H * 60;
  if (H < 0) {
    H += 360;
  }
  return {h: H, s: S, l: L};
}

function colorName(hslcolor) {
  const l = Math.floor(hslcolor.l) * calibration.l; //  empiric coefficient , require calibration
  const s = Math.floor(hslcolor.s) * calibration.s; // empiric coefficient, require calibration
  const h = Math.floor(hslcolor.h);
  let word = 'Undefined';
  if (l < 14) {
    word = 'Black';
    return word;
  }
  if (word !== 'Undefined') {
    return word;
  }
  if (l < 20 && s < 70) {
    if (l < 15 && s < 70) {
      word = 'Black';
    }
    if (l < 18 && s < 50) {
      word = 'Black';
    }
    if (l < 20 && s < 25) {
      word = 'Black';
    }
  }
  if (word !== 'Undefined') {
    return word;
  }
  if (l > 89 && s < 65) {
    if (l >= 91) {
      word = 'White';
    }
    if (l > 91 && s < 65) {
      word = 'White';
    }
    if (l > 90 && s < 55) {
      word = 'White';
    }
    if (l > 89 && s < 45) {
      word = 'White';
    }
  }
  if (word !== 'Undefined') {
    return word;
  }
  if (s < 60) {
    if (l > 89 && s < 60) {
      word = 'Gray';
    }
    if (l > 81 && s < 44) {
      word = 'Gray';
    }
    if (s < 25) {
      word = 'Gray';
    }
  }
  if (word === 'Undefined') {
    if ((h >= 0 && h < 19) || h >= 330) {
      if (l < 50) {
        word = 'Orange';
      } else {
        word = 'Red';
      }
    } else {
      if (h >= 19 && h <= 37) {
        if (l < 50) {
          word = 'Brown';
        } else {
          word = 'Orange';
        }
      } else if (h >= 37 && h <= 50) {
        if (l < 47) {
          word = 'Brown';
        } else {
          word = 'Yellow';
        }
      } else if (h >= 50 && h <= 60) {
        word = 'Yellow';
      } else if (h >= 60 && h <= 145) {
        word = 'Green';
      } else if (h >= 145 && h <= 160) {
        word = 'SeaGreen';
      } else if (h >= 160 && h <= 200) {
        word = 'Skyblue';
      } else if (h >= 200 && h <= 244) {
        word = 'Blue';
      } else if (h >= 244 && h <= 315) {
        if (l < 60) {
          word = 'Violet';
        } else {
          word = 'Pink';
        }
      } else if (h >= 315 && h <= 330) {
        word = 'Magenta';
      }
    }
  }
  return word;
}

function saturation(hslObj) {
  let satur = '';
  let light = '';
  const l = Math.floor(hslObj.l) * calibration.l; // empiric coefficient, require calibration
  const s = Math.floor(hslObj.s);
  if (s < 40) {
    satur = 'Gray';
  }
  if (l < 30) {
    light = 'Dark';
  }
  if (l > 70) {
    light = 'Bright';
  }
  return [satur, light];
}

export {
  convertHexToObj,
  averageColor,
  colorDistance,
  hexToObj,
  hex2Dec,
  hsl,
  colorName,
  saturation,
  countColor,
  average4Points,
};
