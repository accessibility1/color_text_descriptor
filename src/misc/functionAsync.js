/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import {Platform} from 'react-native';
import {sizes} from '../const/pixelPositions.js';
import ImageResizer from '@bam.tech/react-native-image-resizer';
import ImgToBase64 from 'react-native-image-base64';
import {FileSystem} from 'react-native-file-access';

function resize(path) {
  const max = sizes.portrait.ymax;
  if (Platform.OS === 'ios') {
    path = 'file://' + path; // workaround in this library
  }
  const fotoMini = ImageResizer.createResizedImage(
    path,
    max,
    max,
    'JPEG',
    80,
    0,
    undefined,
    false,
    {mode: 'stretch'},
  );
  return fotoMini;
}
function img2base64(pathMini) {
  const uri = pathMini.uri;
  const base64 = ImgToBase64.getBase64String(uri);
  return base64;
}

function base64header(base64) {
  const base64data = 'data:image/jpeg;base64,' + base64;
  return base64data;
}

/* function colorAnalysis(base64data) {
  const resObj = ImageColors.getColors(base64data, {
    fallback: '#000000',
    cache: false,
  });
  return resObj;
}*/

function supplyRes(resObj) {
  console.log('resObj ' + JSON.stringify(resObj));
}

function deletingFile(pathname) {
  //  console.log('deleting ');
  FileSystem.unlink(pathname);
}

export {
  resize,
  img2base64,
  base64header,
  //  colorAnalysis,
  supplyRes,
  deletingFile,
};
