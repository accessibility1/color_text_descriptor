/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import {Camera} from 'react-native-vision-camera';

async function request() {
  try {
    const richiesta = await Camera.requestCameraPermission();
    if (!richiesta) {
      throw new Error();
    }
    return richiesta;
  } catch (error) {
    console.log('error: ' + error);
  }
}
/*
async function elaboraPermessi() {
  try {
    const permission = await Camera.getCameraPermissionStatus();
    if (!permission) {
      throw new Error();
    }
    return permission;
  } catch (error) {
    log.debug('error: ' + error);
  }
}
*/

export {request};
