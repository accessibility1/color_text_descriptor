import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {PermissionPage} from './containers/Permission.js';
import {PhotocameraPage} from './containers/Photocamera.js';
import {styles} from './theme/styles.js';
import {useTranslation} from 'react-i18next';

const App = () => {
  const {t, i18n} = useTranslation();
  const Tab = createBottomTabNavigator();
  // wrap the navigator with a context provider to pass data to the screens (recomended)
  return (
    <NavigationContainer>
      <Tab.Navigator screenOptions={styles.tabStyle}>
        <Tab.Screen
          accessible={true}
          accessibilityLabel={t('Permissions tab')}
          accessibilityLanguage="en/EN"
          name={t('Permission tab')}
          component={PermissionPage}
          options={styles.permissionTabIcon}
        />
        <Tab.Screen
          accessible={true}
          accessibilityLabel={t('Camera tab')}
          accessibilityLanguage="en/EN"
          name={t('Camera tab')}
          component={PhotocameraPage}
          options={styles.cameraTabIcon}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
